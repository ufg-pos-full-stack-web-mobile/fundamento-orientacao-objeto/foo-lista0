package br.ufg.pos.fswm.foo.lista0;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ValidadorCPFTest {

    private ValidadorCPF sut;

    @Before
    public void setUp() throws Exception {
        sut = new ValidadorCPF();
    }

    @Test
    public void deve_dar_ok_para_um_cpf_valido() throws Exception {
        assertTrue(sut.validar("11246115093"));
    }

    @Test
    public void nao_deve_dar_ok_para_um_cpf_invalido() throws Exception {
        assertFalse(sut.validar("11553112112"));
    }
}
