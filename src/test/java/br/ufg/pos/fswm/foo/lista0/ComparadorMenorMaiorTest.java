package br.ufg.pos.fswm.foo.lista0;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Criar um programa que dados 3 valores, mostra o maior e o menor;
 *
 * @author Bruno Nogueira de Oliveira
 * @date 08/04/17.
 */
public class ComparadorMenorMaiorTest {

    private ComparadorMenorMaior sut;

    @Test
    public void deve_receber_tres_valores_como_parametro_e_retornar_o_menor() throws Exception {
        sut = new ComparadorMenorMaior(52, 864, 16);

        assertEquals(16, sut.getMenor());
    }

    @Test
    public void deve_receber_tres_valores_como_parametro_e_retornar_maior() throws Exception {
        sut = new ComparadorMenorMaior(52, 864, 16);

        assertEquals(864, sut.getMaior());
    }
}
