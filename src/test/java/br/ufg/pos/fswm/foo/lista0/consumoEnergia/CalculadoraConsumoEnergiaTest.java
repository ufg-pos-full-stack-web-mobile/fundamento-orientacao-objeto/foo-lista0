package br.ufg.pos.fswm.foo.lista0.consumoEnergia;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraConsumoEnergiaTest {

    private CalculadoraConsumoEnergia sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraConsumoEnergia();
    }

    @Test
    public void deve_calcular_o_valor_da_conta_de_energia_com_10_porcento_de_desconto() throws Exception {
        final double valor = sut.calcular(50.0, 3.50);
        assertEquals(157.5, valor, 0.0001);
    }
}
