package br.ufg.pos.fswm.foo.lista0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ManipuladorStringTest {

    public static final String NOME = "Bruno Nogueira de Oliveira";

    private ManipuladorString sut;

    @Before
    public void setUp() throws Exception {
        sut = new ManipuladorString();
    }

    /*
     * Criar um programa que leia um nome e retorne a quantidade de letras desse nome e o nome em maiúsculo
     */
    @Test
    public void deve_retornar_quantidade_de_caracteres_de_um_nome() throws Exception {
        assertEquals(26, sut.quantidadeCaracteres(NOME));
    }

    @Test
    public void deve_retornar_nome_com_todos_caracteres_em_maiusculo() throws Exception {
        final String nomeEsperado = "BRUNO NOGUEIRA DE OLIVEIRA";

        assertEquals(nomeEsperado, sut.colocarTudoMaiusculo(NOME));
    }

    /*
     * Criar um programa que leia um nome e use todos os métodos mostrados na teoria
     */
    @Test
    public void deve_ser_possivel_comparar_duas_strings_iguais() throws Exception {
        final String nome2 = "Bruno Nogueira de Oliveira";

        assertTrue(sut.saoIguais(NOME, nome2));
    }

    @Test
    public void deve_ser_possivel_comparar_duas_strings_diferentes() throws Exception {
        final String nome2 = "Dayane Vieira da Conceição";

        assertFalse(sut.saoIguais(NOME, nome2));
    }

    @Test
    public void deve_retornar_o_caractere_de_uma_posicao_de_uma_string() throws Exception {
        assertEquals('O', sut.getCaracterePosicao(NOME, 18));
    }

    @Test
    public void deve_retornar_substring_de_uma_string() throws Exception {
        assertEquals("no Nogue", sut.getSubstring(NOME, 3, 11));
    }

    @Test
    public void deve_retornar_com_todos_caracteres_minusculos() throws Exception {
        final String nomeEsperado = "bruno nogueira de oliveira";
        assertEquals(nomeEsperado, sut.colocarTudoMinusculo(NOME));
    }

    @Test
    public void deve_retirar_caracteres_de_espaco_do_final_da_string() throws Exception {
        final String nomeTest = "Bruno Nogueira de Oliveira              ";
        assertEquals(NOME, sut.retirarCaracteresEspacoFinal(nomeTest));
    }

    @Test
    public void deve_trocar_um_caractere_da_string_por_outro() throws Exception {
        final String nomeEsperado = "BrunX NXgueira de Oliveira";
        assertEquals(nomeEsperado, sut.trocarCaractere(NOME, "o", "X"));
    }

    @Test
    public void deve_retornar_a_posicao_de_determinado_caractere_em_uma_string() throws Exception {
        assertEquals(9, sut.posicao(NOME, "ueir"));
    }

    @Test
    public void deve_retornar_a_ultima_posicao_de_determinado_caractere_em_uma_string() throws Exception {
        assertEquals(22, sut.ultimaPosicao(NOME, "e"));
    }

    @Test
    public void deve_ser_possivel_comparar_igualdade_de_duas_strings_ignorando_caso() throws Exception {
        final String nome = "brUnO nOgUEIrA dE OlIvEIrA";

        assertTrue(sut.saoIguaisIgnorandoCase(nome, NOME));
    }
}
