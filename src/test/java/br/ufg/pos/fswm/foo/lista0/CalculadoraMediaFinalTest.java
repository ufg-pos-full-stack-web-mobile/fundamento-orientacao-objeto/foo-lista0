package br.ufg.pos.fswm.foo.lista0;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraMediaFinalTest {

    private CalculadoraMediaFinal sut;

    @Test
    public void deve_calcular_media_final_de_aluno() throws Exception {
        double[]  notas = {5.0, 6.0, 7.0, 8.0};
        sut = new CalculadoraMediaFinal(notas);

        assertEquals(6.555, sut.calcularMedia(), 0.0001);
    }
}
