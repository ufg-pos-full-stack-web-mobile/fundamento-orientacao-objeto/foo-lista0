package br.ufg.pos.fswm.foo.lista0;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraAnoBissextoTest {

    private CalculadoraAnoBissexto sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraAnoBissexto();
    }

    @Test
    public void deve_identificar_ano_bissexto_2004() throws Exception {
        assertTrue(sut.isBissexto(2004));
    }

    @Test
    public void deve_identificar_ano_nao_bissexto_2002() throws Exception {
        assertFalse(sut.isBissexto(2002));
    }

    @Test
    public void deve_identificar_ano_nao_bissexto_2000() throws Exception {
        assertFalse(sut.isBissexto(2000));
    }
}
