package br.ufg.pos.fswm.foo.lista0;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 08/04/17.
 */
public class ListadorNumerosTest {

    private ListadorNumeros sut;

    /*
     * Criar um programa que liste números de 1 a 20;
     */
    @Test
    public void deve_listar_os_numeros_de_1_a_20() throws Exception {
        sut = new ListadorNumeros(1, 20);

        String esperado = "1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20";
        String calculado = sut.listarTodos();

        assertEquals(esperado, calculado);
    }


    /*
     * Criar um programa que liste o quadrado dos números de 1 a 20;
     */
    @Test
    public void deve_listar_os_quadrados_dos_numeros_de_1_a_20() throws Exception {
        sut = new ListadorNumeros(1, 20);

        String esperado = "1 4 9 16 25 36 49 64 81 100 121 144 169 196 225 256 289 324 361 400";
        String calculado = sut.listarQuadrados();

        assertEquals(esperado, calculado);
    }
    
    /*
     * Criar um programa que liste os números impares de 1 a 50;
     */

    @Test
    public void deve_listar_os_impares_dos_numeros_de_1_a_50() throws Exception {
        sut = new ListadorNumeros(1, 50);

        String esperado = "1 3 5 7 9 11 13 15 17 19 21 23 25 27 29 31 33 35 37 39 41 43 45 47 49";
        String calculado = sut.listarImpares();

        assertEquals(esperado, calculado);
    }

    /*
     * Criar um programa que liste a soma dos números de 1 a 50;
     */
    @Test
    public void deve_retornar_a_soma_de_todos_elementos_de_1_a_50() throws Exception {
        sut = new ListadorNumeros(1, 50);

        assertEquals(1275, sut.somarTodosElementos());
    }
    
    /*
     * Criar um programa que liste o fatorial dos números de 1 a 10;
     */

    @Test
    public void deve_listar_todos_os_fatoriais_de_1_a_10() throws Exception {
        sut = new ListadorNumeros(1, 10);

        String esperado = "1 2 6 24 120 720 5040 40320 362880 3628800";
        String calculado = sut.listarFatoriais();

        assertEquals(esperado, calculado);
    }

    @Test
    public void deve_retornar_a_soma_de_todos_elementos_impares_de_0_a_100() throws Exception {
        sut = new ListadorNumeros(0, 100);

        assertEquals(2500, sut.somarTodosElementosImpares());
    }
}
