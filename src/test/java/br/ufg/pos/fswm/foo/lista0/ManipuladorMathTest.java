package br.ufg.pos.fswm.foo.lista0;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ManipuladorMathTest {

    private ManipuladorMath sut;

    @Before
    public void setUp() throws Exception {
        sut = new ManipuladorMath();
    }

    /*
     * Criar um programa que leia um valor decimal e retorne esse valor arredondado usando os três
     * métodos de arredondamento da classe Math
     */
    @Test
    public void deve_ler_um_decimal_e_retornar_os_3_valores_arrendondados() throws Exception {
        final String esperado = "2,0 3,0 2,0";
        assertEquals(esperado, sut.todosArredondamentos(2.4));
    }

    /*
     * Criar um programa que leia dois números e retorne o maior e o menor
     */
    @Test
    public void deve_retornar_o_maior_de_dois_numeros_recebidos() throws Exception {
        assertEquals(56, sut.maiorDeDois(56, 14));
    }

    @Test
    public void deve_retornar_o_menor_de_dois_numeros_recebidos() throws Exception {
        assertEquals(14, sut.menorDeDois(56, 14));
    }

    /*
     * Criar um programa que retorne um número aleatório de 0 a 100.
     */
    @Test
    public void deve_retornar_um_numero_aletario_entre_0_e_100() throws Exception {
        int aleatorio = sut.aleatorio();

        System.out.println(aleatorio);

        assertTrue(aleatorio > 0 && aleatorio < 100);
    }

    /*
     * Criar um programa que leia um número e retorne o seu quadrado e a sua raiz.
     */
    @Test
    public void deve_retornar_o_quadrado_e_a_raiz_de_um_numero() throws Exception {
        final String esperado = "Quadrado = 81,0\nRaiz = 3,0";

        assertEquals(esperado, sut.quadradoERaiz(9.0));
    }

    /*
     * Criar um programa que leia 2 valores e mostre a soma, a subtração, a multiplicação e a divisão
     * entre eles. (A+B, A-B, A*B, A/B)
     */
    @Test
    public void deve_efetuar_as_operacoes_matematicas_basicas_para_dado_dois_numeros() throws Exception {
        final String resultado = "Soma = 75,0\nSubtração = 25,0\nMultiplicação = 1250,0\nDivisão = 2,0";
        assertEquals(resultado, sut.todasOperacoes(50.0, 25.0));
    }
}
