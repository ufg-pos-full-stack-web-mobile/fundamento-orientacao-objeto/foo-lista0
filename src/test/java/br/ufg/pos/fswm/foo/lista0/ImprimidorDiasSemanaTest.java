package br.ufg.pos.fswm.foo.lista0;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ImprimidorDiasSemanaTest {

    private ImprimidorDiasSemana sut;

    @Before
    public void setUp() throws Exception {
        sut = new ImprimidorDiasSemana();
    }

    @Test
    public void deve_retornar_DOMINGO_quando_receber_valor_1() throws Exception {
        assertEquals("DOMINGO", sut.imprimirDiaSemana(1));
    }

    @Test
    public void deve_retornar_SEGUNDA_quando_receber_valor_2() throws Exception {
        assertEquals("SEGUNDA", sut.imprimirDiaSemana(2));
    }

    @Test
    public void deve_retornar_TERCA_quando_receber_valor_3() throws Exception {
        assertEquals("TERÇA", sut.imprimirDiaSemana(3));
    }

    @Test
    public void deve_retornar_QUARTA_quando_receber_valor_4() throws Exception {
        assertEquals("QUARTA", sut.imprimirDiaSemana(4));
    }

    @Test
    public void deve_retornar_QUINTA_quando_receber_valor_5() throws Exception {
        assertEquals("QUINTA", sut.imprimirDiaSemana(5));
    }

    @Test
    public void deve_retornar_SEXTA_quando_receber_valor_6() throws Exception {
        assertEquals("SEXTA", sut.imprimirDiaSemana(6));
    }

    @Test
    public void deve_retornar_SABADO_quando_receber_valor_7() throws Exception {
        assertEquals("SÁBADO", sut.imprimirDiaSemana(7));
    }
}
