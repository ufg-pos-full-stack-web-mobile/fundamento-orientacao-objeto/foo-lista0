package br.ufg.pos.fswm.foo.lista0;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraBaskaraTest {

    private CalculadoraBaskara sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraBaskara(1, 5, 4);
    }

    @Test
    public void deve_calcular_X1() throws Exception {
        assertEquals(-1.0, sut.calcularX1(), 0.001);
    }

    @Test
    public void deve_calcular_X2() throws Exception {
        assertEquals(-4.0, sut.calcularX2(), 0.001);
    }
}
