package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraBaskara {
    private final int c;
    private final int b;
    private final int a;

    public CalculadoraBaskara(int a, int b , int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private double calcularDelta() {
        return b*b - 4*a*c;
    }

    public double calcularX1() {
        double delta = calcularDelta();

        return (-b + Math.sqrt(delta)) / (2 * a);
    }

    public double calcularX2() {
        double delta = calcularDelta();

        return (-b - Math.sqrt(delta)) / (2*a);
    }
}
