package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ManipuladorMath {
    public String todosArredondamentos(double valor) {
        StringBuilder sb = new StringBuilder();

        String paraBaixo = String.format("%.1f ", Math.floor(valor));
        sb.append(paraBaixo);

        String paraCima = String.format("%.1f ", Math.ceil(valor));
        sb.append(paraCima);

        String arrendondado = String.format("%.1f", (double) Math.round(valor));
        sb.append(arrendondado);

        return sb.toString();
    }

    public int maiorDeDois(int num1, int num2) {
        return Math.max(num1, num2);
    }

    public int menorDeDois(int num1, int num2) {
        return Math.min(num1, num2);
    }

    public int aleatorio() {
        return (int) (Math.random() * 100);
    }

    public String quadradoERaiz(double numero) {
        return String.format("Quadrado = %.1f\nRaiz = %.1f", quadrado(numero), raiz(numero));
    }

    private double raiz(double numero) {
        return Math.sqrt(numero);
    }

    private double quadrado(double numero) {
        return Math.pow(numero, 2);
    }

    public String todasOperacoes(double num1, double num2) {
        return String.format("Soma = %.1f\nSubtração = %.1f\nMultiplicação = %.1f\nDivisão = %.1f",
                num1 + num2, num1 - num2, num1 * num2, num1 / num2);
    }
}
