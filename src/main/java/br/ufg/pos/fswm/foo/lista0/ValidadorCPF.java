package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ValidadorCPF {

    public boolean validar(String cpf) {

        int soma = 0;
        int multiplicador = 10;
        for(int i = 0 ; i < 9 ; i++) {
            int num = Integer.parseInt(String.valueOf(cpf.charAt(i)));
            soma += num * multiplicador--;
        }
        int digitoVerificador = 0;
        int resto = soma % 11;
        if(resto <= 9) {
            digitoVerificador = 11 - resto;
        }
        if(!String.valueOf(cpf.charAt(9)).equals(String.valueOf(digitoVerificador))) {
            return false;
        }

        soma = 0;
        multiplicador = 11;
        for(int i = 0 ; i < 10 ; i++) {
            int num = Integer.parseInt(String.valueOf(cpf.charAt(i)));
            soma += num * multiplicador--;
        }
        digitoVerificador = 0;
        resto = soma % 11;
        if(resto <= 9) {
            digitoVerificador = 11 - resto;
        }
        if(!String.valueOf(cpf.charAt(10)).equals(String.valueOf(digitoVerificador))) {
            return false;
        }

        return true;
    }
}
