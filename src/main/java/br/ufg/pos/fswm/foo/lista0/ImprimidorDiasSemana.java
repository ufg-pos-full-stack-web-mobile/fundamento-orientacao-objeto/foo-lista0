package br.ufg.pos.fswm.foo.lista0;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ImprimidorDiasSemana {

    public String imprimirDiaSemana(int diaInt) {
        String diaSemana = null;

        switch (diaInt) {
            case 1:
                diaSemana = "DOMINGO";
                break;
            case 2:
                diaSemana = "SEGUNDA";
                break;
            case 3:
                diaSemana = "TERÇA";
                break;
            case 4:
                diaSemana = "QUARTA";
                break;
            case 5:
                diaSemana = "QUINTA";
                break;
            case 6:
                diaSemana = "SEXTA";
                break;
            case 7:
                diaSemana = "SÁBADO";
                break;
        }
        return diaSemana;
    }

    public static void main(String... args) {
        ImprimidorDiasSemana imprimidor = new ImprimidorDiasSemana();

        int diaInt = capturarNumInteiro();

        JOptionPane.showMessageDialog(null, "Dia da semana: " + imprimidor.imprimirDiaSemana(diaInt));
    }

    private static int capturarNumInteiro() {
        boolean valido;
        int num = 0;
        valido = false;
        do {
            String numStr = JOptionPane.showInputDialog("Digite um número");
            try {
                num = Integer.parseInt(numStr);
                valido = true;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Digite um número válido");
            }
        } while (!valido);

        return num;
    }
}
