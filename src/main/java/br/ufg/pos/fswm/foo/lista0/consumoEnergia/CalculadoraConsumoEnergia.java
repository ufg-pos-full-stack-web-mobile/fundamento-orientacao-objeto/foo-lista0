package br.ufg.pos.fswm.foo.lista0.consumoEnergia;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraConsumoEnergia {
    public double calcular(double quantidadeKiloWatts, double valorKiloWatts) {
        final double valorTotal = quantidadeKiloWatts * valorKiloWatts;
        return valorTotal - (valorTotal * 0.1);
    }
}
