package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 14/04/17.
 */
public class CalculadoraAnoBissexto {
    public boolean isBissexto(int ano) {
        if(ano % 4 == 0 && ano % 100 != 0) {
            return true;
        }
        return false;
    }
}
