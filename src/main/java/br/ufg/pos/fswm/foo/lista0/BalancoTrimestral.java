package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class BalancoTrimestral {

    public static void main(String... args) {
        int gastosJaneiro = 15000;
        int gastosFevereiro = 23000;
        int gastosMarco = 17000;

        int gastosTrimestrais = gastosJaneiro + gastosFevereiro + gastosMarco;

        System.out.println("O gasto trimestral foi de " + gastosTrimestrais);

        int mediaMensal = gastosTrimestrais / 3;

        System.out.println("Valor da média mensal = " + mediaMensal);
    }

}
