package br.ufg.pos.fswm.foo.lista0;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ImprimirImparesDe2a100 {

    public static void main(String... args) {
        ListadorNumeros listador = new ListadorNumeros(2, 100);

        JOptionPane.showMessageDialog(null, listador.listarImpares());
    }
}
