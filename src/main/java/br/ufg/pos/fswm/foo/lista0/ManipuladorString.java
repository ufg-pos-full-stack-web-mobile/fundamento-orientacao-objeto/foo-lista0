package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ManipuladorString {
    public int quantidadeCaracteres(String nome) {
        return nome.length();
    }

    public String colocarTudoMaiusculo(String nome) {
        return nome.toUpperCase();
    }

    public boolean saoIguais(String nome1, String nome2) {
        return nome1.equals(nome2);
    }

    public char getCaracterePosicao(String nome, int posicao) {
        return nome.charAt(posicao);
    }

    public String getSubstring(String nome, int posInicial, int posFinal) {
        return nome.substring(posInicial, posFinal);
    }

    public String colocarTudoMinusculo(String nome) {
        return nome.toLowerCase();
    }

    public String retirarCaracteresEspacoFinal(String nome) {
        return nome.trim();
    }

    public String trocarCaractere(String nome, String alvo, String nova) {
        return nome.replace("o", "X");
    }

    public int posicao(String nome, String strProcurada) {
        return nome.indexOf(strProcurada);
    }

    public int ultimaPosicao(String nome, String strProcurada) {
        return nome.lastIndexOf(strProcurada);
    }

    public boolean saoIguaisIgnorandoCase(String nome, String nome1) {
        return nome.equalsIgnoreCase(nome1);
    }
}
