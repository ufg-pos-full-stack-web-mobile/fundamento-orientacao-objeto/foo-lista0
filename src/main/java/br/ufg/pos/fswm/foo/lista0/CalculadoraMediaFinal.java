package br.ufg.pos.fswm.foo.lista0;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraMediaFinal {

    private final double[] notas;

    public CalculadoraMediaFinal(double[] notas) {
        this.notas = notas;
    }

    public double calcularMedia() {
        return (notas[0] * 0.2 + notas[1] * 0.3 + notas[2] * 0.5) * 0.85 + notas[3] * 0.15;
    }

    public static void main(String... args) {
        double nota1 = informarNota("n1");
        double nota2 = informarNota("n2");
        double nota3 = informarNota("n3");
        double nota4 = informarNota("nt");

        double[] notas = {nota1, nota2, nota3, nota4};

        double notaFinal = new CalculadoraMediaFinal(notas).calcularMedia();

        JOptionPane.showMessageDialog(null, String.format("Nota final de Introdução à Computação: %.3f", notaFinal));

    }

    private static double informarNota(String idNota) {
        boolean isInvalido = true;
        double nota = 0.0;

        do {
            String notaStr = JOptionPane.showInputDialog("Informe a nota " + idNota);
            try {
                nota = Double.parseDouble(notaStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "O valor informado não é valido. Informe um valor válido");
            }

        } while (isInvalido);

        return nota;
    }
}
