package br.ufg.pos.fswm.foo.lista0;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CadastroPessoa {

    public static void main(String... args) {
        String nome = entrarNome();
        int telefone = entrarTelefone();

        JOptionPane.showMessageDialog(null, "Nome: " + nome + "\nTelefone" + telefone);
    }

    private static int entrarTelefone() {
        boolean isTelefoneInvalido = true;
        int telefone = 0;
        do {
            String telefoneStr = JOptionPane.showInputDialog("Digite o telefone:");

            isTelefoneInvalido = !isTelefoneValido(telefoneStr);
            if(!isTelefoneInvalido) {
                telefone = Integer.parseInt(telefoneStr);
            }
        }while(isTelefoneInvalido);


        return telefone;
    }

    private static boolean isTelefoneValido(String telefoneStr) {
        for(int i = 0 ; i < telefoneStr.length() ; i++) {
            if(!Character.isDigit(telefoneStr.charAt(i))) {
                JOptionPane.showMessageDialog(null, "Telefone com caractere alfabético. Tente outro telefone:");
                return false;
            }
        }
        return true;
    }

    private static String entrarNome() {
        boolean isNomeInvalido = true;
        String nome = null;
        do {
            nome = JOptionPane.showInputDialog("Digite o nome:");

            isNomeInvalido = !isNomeValido(nome);
        } while(isNomeInvalido);

        return nome;
    }

    private static boolean isNomeValido(String nome) {
        for(int i = 0 ; i < nome.length() ; i++) {
            if(Character.isDigit(nome.charAt(i))) {
                JOptionPane.showMessageDialog(null, "Nome com caractere numérico. Tente outro nome;");
                return false;
            }
        }
        return true;
    }

}
