package br.ufg.pos.fswm.foo.lista0.consumoEnergia;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class CalculadoraConsumoEnergiaMain {

    public static void main(String... args) {
        double quantidadeKW = 0;
        double valorKW = 0;
        boolean valido = false;

        do {
            String quantidadeKWStr = JOptionPane.showInputDialog("Digite a quantidade de KW consumido:");
            try {
                quantidadeKW = Double.parseDouble(quantidadeKWStr);
                valido = true;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Quantidade inválida. Digite um valor válido");
            }
        } while(!valido);

        valido = false;

        do {
            String valorKWStr = JOptionPane.showInputDialog("Digite o valor do KW:");
            try {
                valorKW = Double.parseDouble(valorKWStr);
                valido = true;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Valor inválido. Digite um valor válido");
            }
        } while(!valido);

        CalculadoraConsumoEnergia calculadora = new CalculadoraConsumoEnergia();
        double valorTotal = calculadora.calcular(quantidadeKW, valorKW);

        JOptionPane.showMessageDialog(null, String.format("Valor da conta de energia = R$ %.2f", valorTotal) );

    }
}
