package br.ufg.pos.fswm.foo.lista0;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 08/04/17.
 */
public class ListadorNumeros {

    private final int valorInicial;
    private final int valorFinal;

    public ListadorNumeros(int valorInicial, int valorFinal) {
        this.valorInicial = valorInicial;
        this.valorFinal = valorFinal;
    }

    public String listarTodos() {
        StringBuilder sb = new StringBuilder();

        for(int i = valorInicial ; i <= valorFinal ; i++) {
            sb.append(i);

            if(i != valorFinal) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    public String listarQuadrados() {
        StringBuilder sb = new StringBuilder();

        for(int i = valorInicial ; i <= valorFinal ; i++) {
            sb.append(i * i);

            if(i != valorFinal) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    public String listarImpares() {
        StringBuilder sb = new StringBuilder();

        for(int i = valorInicial ; i <= valorFinal ; i++) {
            if(i % 2 != 0) {
                sb.append(i);
                if(valorFinal -  1 != i) {
                    sb.append(" ");
                }
            }
        }

        return sb.toString();
    }

    public int somarTodosElementos() {
        int soma = 0;

        for(int i = valorInicial ; i <= valorFinal ; i++) {
            soma += i;
        }

        return soma;
    }

    public String listarFatoriais() {
        StringBuilder sb = new StringBuilder();

        for(int i = valorInicial ; i <= valorFinal ; i++) {
            sb.append(calcularFatorial(i));
            if(i != valorFinal) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    private int calcularFatorial(int i) {
        if(i == 1) {
            return 1;
        } else {
            return i * calcularFatorial(i - 1);
        }
    }

    public int somarTodosElementosImpares() {
        int soma = 0;

        for(int i = valorInicial ; i <= valorFinal ; i++) {
            final boolean isImpar = (i % 2 != 0);
            if(isImpar) {
                soma += i;
            }
        }

        return soma;
    }
}
