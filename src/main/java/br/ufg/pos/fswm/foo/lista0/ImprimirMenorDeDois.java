package br.ufg.pos.fswm.foo.lista0;

import javax.swing.*;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 09/04/17.
 */
public class ImprimirMenorDeDois {

    public static void main(String... args) {
        int num1 = 0;
        int num2 = 0;

        num1 = capturarNumInteiro();
        num2 = capturarNumInteiro();

        if(num1 < num2) {
            JOptionPane.showMessageDialog(null, "O menor número é: " + num1);
        } else {
            JOptionPane.showMessageDialog(null, "O menor número é: " + num2);
        }

    }

    private static int capturarNumInteiro() {
        boolean valido;
        int num = 0;
        valido = false;
        do {
            String numStr = JOptionPane.showInputDialog("Digite um número");
            try {
                num = Integer.parseInt(numStr);
                valido = true;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Digite um número válido");
            }
        } while (!valido);

        return num;
    }
}
