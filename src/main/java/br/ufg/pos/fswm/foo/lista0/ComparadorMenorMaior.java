package br.ufg.pos.fswm.foo.lista0;

import java.util.Arrays;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 08/04/17.
 */
public class ComparadorMenorMaior {

    private final List<Integer> valores;

    public ComparadorMenorMaior(int num1, int num2, int num3) {
        this.valores = Arrays.asList(num1, num2, num3);
    }

    public int getMenor() {
        int menor = Integer.MAX_VALUE;

        for(int valor : valores) {
            if(valor < menor) {
                menor = valor;
            }
        }

        return menor;
    }

    public int getMaior() {
        int maior = Integer.MIN_VALUE;

        for(int valor : valores) {
            if(valor > maior) {
                maior = valor;
            }
        }

        return maior;
    }
}
